-- Gera��o de Modelo f�sico
-- Sql ANSI 2003 - brModelo.



CREATE TABLE Post (
id_post int PRIMARY KEY AUTO_INCREMENT,
titulo_post varchar(80),
texto_post varchar(999999),
id_user int
);

CREATE TABLE Sugestoes (
titulo_sug varchar(80),
id_sug int PRIMARY KEY AUTO_INCREMENT,
texto_sug varchar(1000),
id_user int
);

CREATE TABLE tag (
id_tag int PRIMARY KEY AUTO_INCREMENT,
exemplo varchar(999999),
texto_tag varchar(2000),
titulo_tag varchar(80),
resultado varchar(10000)
);

CREATE TABLE ling_prog (
id_ling int PRIMARY KEY AUTO_INCREMENT,
nome_ling varchar(80),
texto_ling varchar(10000),
id_user int
);

CREATE TABLE noticias (
titulo_not varchar(240),
data date,
texto_not varchar(5000),
id_not int PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE Comentarios (
id_coment int PRIMARY KEY AUTO_INCREMENT,
texto_contario varchar(1000),
titulo_coment varchar(80),
id_user int,
id_post int,
id_tag int,
FOREIGN KEY(id_post) REFERENCES Post (id_post),
FOREIGN KEY(id_tag) REFERENCES tag (id_tag)
);

CREATE TABLE usuario (
id_user int PRIMARY KEY AUTO_INCREMENT,
profissao varchar(30),
email varchar(30),
apelido varchar(30),
senha varchar(40),
nome varchar(30),
id_tipo int
);

CREATE TABLE tip_user (
id_tipo int PRIMARY KEY AUTO_INCREMENT,
desc_tipo varchar(1000)
);

CREATE TABLE tag_ling(
id_ling int,
id_tag int,
FOREIGN KEY(id_ling) REFERENCES ling_prog (id_ling),
FOREIGN KEY(id_tag) REFERENCES tag (id_tag)
);

CREATE TABLE ling_not (
id_ling int,
id_not int,
FOREIGN KEY(id_ling) REFERENCES ling_prog (id_ling),
FOREIGN KEY(id_not) REFERENCES noticias (id_not)
);

ALTER TABLE Post ADD FOREIGN KEY(id_user) REFERENCES usuario (id_user);
ALTER TABLE Sugestoes ADD FOREIGN KEY(id_user) REFERENCES usuario (id_user);
ALTER TABLE ling_prog ADD FOREIGN KEY(id_user) REFERENCES usuario (id_user);
ALTER TABLE Comentarios ADD FOREIGN KEY(id_user) REFERENCES usuario (id_user);
ALTER TABLE usuario ADD FOREIGN KEY(id_tipo) REFERENCES tip_user (id_tipo);
