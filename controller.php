<?php 
include_once'conexao.php';

function getTemplate($template) {
	if ( is_file( $template) ) { // verificando se o arq existe
		return file_get_contents( $template ); // retornando conteúdo do arquivo
	} else{
		return FALSE;
	}
}
####################################################################

function parseTemplate( $template, $vetor ) {	
	foreach ($vetor as $a => $b) { // recebemos um array com as tags
		$template = str_replace( '{'.$a.'}', $b, $template );
	}
	return $template; // retorno o html com conteúdo final
}

####################################################################

function parseTemplates( $template, $vetors ) { //pega template e array
	 foreach ($vetors as $cont ) { //percorre array atribui o valor do elemento e atribui a $cont
        foreach ($cont as $key => $val) { //$cont é outro array que é percorrido e atribui a posição atual do array a $key e o valor da posição a $val
            $template = str_replace('{'.$key.'}', $val, $template); //procura pelo valor de $key dentro de $templates e troca pelo valor de $val dentro de $templates
        }
    }
    return $templates;
}

####################################################################

function obterProduto($cons){
	$conexao = obterConexao();
	$consulta = $conexao->query("$cons");
	$produtos = $consulta->fetchAll(PDO::FETCH_ASSOC);
	return $produtos;
}
####################################################################

function ins_dados($pos,$pgs){
    switch ($pos){
        case 1:
            include_once $pgs;
            break;
        case 2:
            include_once $pgs;
            break;
        case 3:
            include_once $pgs;
            break;
        default :
            include_once'templates/home.php' ;
            break;
    }
}
###################################################################

