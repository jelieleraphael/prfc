<?php 
include_once 'controller.php'; ?>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Programação IFC</title>

    <style>
        .img-meu{
            width: auto;
            max-height: 400px;
            margin: auto;
        }
    </style>
    <link href="bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="Flat-UI-master/dist/css/flat-ui.min.css" rel="stylesheet">
    <link href="../css2.css" rel="stylesheet">
    <script src="bootstrap-3.3.6-dist/js/jquery.min.js"></script> 
    <script src="bootstrap-3.3.6-dist/js/bootstrap.js"></script>
    <script src="bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>          
    <script src="bootstrap-3.3.6/js/dropdown.js"></script>
    <script src="bootstrap-3.3.6/js/tab.js"></script>
    <script src="bootstrap-3.3.6/js/carousel.js"></script>
</head>

<body class="container-fluid">        
    <header id="navbar-example" class="navbar navbar-inverse navbar-fixed-top">
        <?php
            $menu = ['menu1a'=>'index.php?pos=2&pgs=templates/cadu.php',
            'menu1b'=>'index.php?pos=2&pgs=templates/suges.php',
            'menu1c'=>'index.php?pos=2&pgs=templates/ocup.php',
           
            'menu2a'=>'index.php?pos=2&pgs=controladores/select_user.php',
            'menu2b'=>'index.php?pos=2&pgs=templates/editL.php',

            /*'menu2c'=>'Teste3',*/
                            
            'menu3a'=>'index.php?pos=2&pgs=templates/html.php',
            'menu3b'=>'index.php?pos=2&pgs=templates/css.php',
            'menu3c'=>'index.php?pos=2&pgs=templates/php.php'
            ];

            $template1 = getTemplate('templates/menu.php');
            $templateFinal = parseTemplate( $template1, $menu );
            echo $templateFinal;
        ?>
    </header> <!-- /navbar-example -->
    <section class="container">
        <?php ins_dados(filter_input(INPUT_GET, 'pos'),filter_input(INPUT_GET, 'pgs')); ?>
        <hr>

        <footer>
            <p>Layout Bootstrap - Conteudos de Autoria de Jeliel Braz e Raphael Kammer - 2016</p>
        </footer>
    </section>

</body>
</html>

