<nav class="container-fluid">
    <section class="navbar-header">
        <button class="navbar-toggle " type="button" data-toggle="collapse" data-target=".bs-example-js-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">- Programação IFC -</a>
    </section>
    <section class="collapse navbar-collapse bs-example-js-navbar-collapse">
        <ul class="nav navbar-nav navbar">
            <li class="dropdown">
                <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Usuário
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="drop1">
                    <li><a href="{menu1a}">Cadastro Usuario</a></li>
                    <li><a href="{menu1b}">Sugestões</a></li>
                </ul>
            </li>

            <li class="dropdown">
                <a id="drop2" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Linguagens
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="drop2">
                    <li><a href="{menu3a}">HTML</a></li>
                    <li><a href="{menu3b}">CSS</a></li>
                    <li><a href="{menu3c}">PHP</a></li>
                </ul>
            </li>

            <li class="dropdown">
                <a id="drop2" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    Admin
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu" aria-labelledby="drop2">
                    <li><a href="{menu2a}">Lista de Usuário</a></li>
                    <li><a href="{menu2b}">Cadastro de Tags</a></li>

                </ul>
            </li>
        </ul>
        <form class="navbar-form navbar-right" action="#" role="login">
            <section class="form-group">
                <span class="input-group">
                    <input id="navbarInput-01" class="form-control" placeholder="login" type="text"/>  
                    <span class="input-group-btn">
                        <button class="btn">
                            <span class="fui-mail"></span>
                        </button>
                    </span>
                </span>
            </section>
            <section class="form-group"> 
                <span class="input-group">                                    
                    <input id="navbarInput-01" class="form-control" placeholder="password" type="password"/>
                    <span class="input-group-btn">
                        <button class="btn">
                            <span class="fui-lock"></span>
                        </button>      
                    </span>
                </span>
            </section>
        </form>
    </section><!-- /.nav-collapse -->
</nav><!-- /.container-fluid -->